const express = require("express"),
    connection = require("../db/config"),
    user = require("../models/user");

let router = express.Router();

router.get("/users", (req, res) => {
    user.find({}).exec().then((docs) => {
        return res.render("userList", { Title: "User List", data: docs });
    }).catch((err) => {
        console.log(err);
        res.render("userList", { Title: "User List", data: [] });
    });
});

router.get("/user/create", (req, res) => {
    res.render("createUser", { Title: "Create User" });
}).post("/user/create", (req, res) => {
    let body = req.body;
    if (body != undefined) {
        let obj = new user({
            username: body.username,
            name: body.name,
            designation: body.designation,
            address: body.address
        });
        obj.save((err) => {
            if (err) {
                console.log(err);
                res.render("createUser", { Title: "Create User" });
            }
            res.redirect("/users");
        });
    } else {
        res.render("createUser", { Title: "Create User" });
    }
});

router.get("/user/edit/:id", (req, res) => {
    let id = req.params.id;
    user.findById(id).exec().then((doc) => {
        return res.render("editUser", { Title: "Edit User", user: doc });
    }).catch((err) => {
        res.render("editUser", { Title: "Edit User" });
    });
}).post("/user/edit", (req, res) => {
    let body = req.body;
    if (body != undefined) {
        user.findByIdAndUpdate(body._id, { name: body.name, username: body.username ,address: body.address, designation: body.designation }).exec().then(() => {
            return res.redirect("/users");
        }).catch((err) => {
            res.render("editUser", { Title: "Edit User" });
        });
    } else {
        res.render("editUser", { Title: "Edit User" });
    }
});

router.get("/user/delete/:id", (req, res) => {
    let id = req.params.id;
    user.findByIdAndRemove(id).exec().then(() => {
        return res.redirect("/users");
    }).catch((err) => {
        return res.redirect("/users");
    });
});

module.exports = router;