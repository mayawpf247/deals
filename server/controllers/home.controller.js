const express = require("express");
let router = express.Router();

router.get("/", (req, res) => {
    res.render("home", { Title: "Home Page" });
});
router.get("/about", (req, res) => {
    res.render("about", { Title: "About Us Page" });
});

module.exports = router;