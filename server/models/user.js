const mongoose = require("mongoose");

let Schema = mongoose.Schema,
    ObjectId = Schema.ObjectId;

let userSchema = new Schema({
    _id: { type: ObjectId, auto: true },
    username: String,
    name: String,
    address: String,
    designation: String
}, { versionKey: false });

let model = mongoose.model("users", userSchema);
module.exports = model;